package KeThua;

import java.util.Scanner;

public abstract class TaiLieu {
    private Scanner sc = new Scanner(System.in);
    protected String id;
    protected String name;
    protected String publisher;

    public TaiLieu(String id, String name, String publisher) {
        this.id = id;
        this.name = name;
        this.publisher = publisher;
    }
    public abstract void nhapInfo();
    public abstract void xuatInfo();

    public String getPublisher() {
        return publisher;
    }

    public abstract String getPublishYear();
}
