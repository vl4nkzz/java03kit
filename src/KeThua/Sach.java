package KeThua;

import java.util.Scanner;

public class Sach extends TaiLieu {
    private Scanner sc = new Scanner(System.in);
    public Sach(String id, String name, String publisher) {
        super(id, name, publisher);
    }
    protected int pageAmount;
    protected String publishYear;
    protected String edition;
    protected String author;

    @Override
    public String getPublishYear() {
        return publishYear;
    }

    @Override
    public void nhapInfo(){
        System.out.println("Nhập mã: ");
        this.id = sc.nextLine();
        System.out.println("Nhập tên: ");
        this.name = sc.nextLine();
        System.out.println("Nhập tên NXB: ");
        this.publisher = sc.nextLine();
        System.out.println("Nhập số trang: ");
        this.pageAmount = sc.nextInt();
        sc.nextLine();
        System.out.println("Nhập năm xuất bản: ");
        this.publishYear = sc.nextLine();
        System.out.println("Nhập lần tái bản: ");
        this.edition = sc.nextLine();
        System.out.println("Nhập tác giả: ");
        this.author = sc.nextLine();
    }

    @Override
    public void xuatInfo(){
        System.out.println("Sách");
        System.out.println("Mã: "+this.id);
        System.out.println("Tên: "+this.name);
        System.out.println("NXB: "+this.publisher);
        System.out.println("Số trang: "+this.pageAmount);
        System.out.println("Năm xuất bản: "+this.publishYear);
        System.out.println("Lần tái bản: "+this.edition);
        System.out.println("Tác giả: "+this.author);
    }
}
