package KeThua;

import java.util.Scanner;

public class ConNguoi {
    private Scanner sc = new Scanner(System.in);
    protected String name;
    protected int bornyear;
    protected String hometown;
    protected String sex;

    public ConNguoi(String name, int bornyear, String hometown, String sex) {
        this.name = name;
        this.bornyear = bornyear;
        this.hometown = hometown;
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void nhapInfo(){
        System.out.println("Nhập tên: ");
        this.name = sc.nextLine();
        System.out.println("Nhập năm sinh: ");
        this.bornyear = sc.nextInt();
        sc.nextLine();
        System.out.println("Nhập quê quán: ");
        this.hometown = sc.nextLine();
        System.out.println("Nhập giới tính: ");
        this.sex = sc.nextLine();
    }

    public void xuatInfo(){
        System.out.println("Tên: "+this.name);
        System.out.println("Năm sinh: "+this.bornyear);
        System.out.println("Quê quán: "+this.hometown);
        System.out.println("Giới tính: "+this.sex);
    }
}
