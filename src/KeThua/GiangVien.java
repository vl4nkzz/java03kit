package KeThua;

import java.util.Scanner;

public class GiangVien extends ConNguoi {
    private Scanner sc = new Scanner(System.in);
    protected String major;
    protected String subject;
    protected int yearexp;

    public GiangVien(String name, int bornyear, String hometown, String sex) {
        super(name, bornyear, hometown, sex);
    }

    public String getMajor() {
        return major;
    }

    @Override
    public void nhapInfo(){
        super.nhapInfo();
        System.out.println("Nhập khoa: ");
        this.major = sc.nextLine();
        System.out.println("Nhập môn: ");
        this.subject = sc.nextLine();
        System.out.println("Nhập năm kinh nghiệm: ");
        this.yearexp = sc.nextInt();
        sc.nextLine();
    }
    @Override
    public void xuatInfo(){
        super.xuatInfo();
        System.out.println("Khoa: "+this.major);
        System.out.println("Môn: "+this.subject);
        System.out.println("Năm kinh nghiệm: "+this.yearexp);
    }
}
