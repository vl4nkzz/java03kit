package KeThua;

import java.util.Scanner;

public class HocSinh extends ConNguoi {
    private Scanner sc = new Scanner(System.in);
    protected String classID;
    protected String majors;
    protected String ger;
    public HocSinh(String name, int bornyear, String hometown, String sex){
        super(name,bornyear,hometown,sex);
    }

    public String getMajors() {
        return majors;
    }



    @Override
    public void nhapInfo(){
        super.nhapInfo();
        System.out.println("Nhập mã lớp: ");
        this.classID = sc.nextLine();
        System.out.println("Nhập ngành học: ");
        this.majors = sc.nextLine();
        System.out.println("Nhập khoá: ");
        this.ger = sc.nextLine();
    }

    @Override
    public void xuatInfo(){
        super.xuatInfo();
        System.out.println("Mã lớp: "+this.classID);
        System.out.println("Ngành học: "+this.majors);
        System.out.println("Khoá: "+this.ger);

    }

}
