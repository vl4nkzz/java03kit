package KeThua;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TapChi extends TaiLieu {
    private Scanner sc = new Scanner(System.in);
    public TapChi(String id, String name, String publisher) {
        super(id, name, publisher);
    }
    protected String publishDay;
    protected String publishYear;
    protected int pageAmount;
    protected String publishCat;

    private void setPublishYear(){
        SimpleDateFormat newformat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        try {
            Date day = newformat.parse(this.publishDay);
            this.publishYear = year.format(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getPublishYear() {
        return publishYear;
    }

    @Override
    public void nhapInfo(){
        System.out.println("Nhập thông tin tạp chí");
        System.out.println("Nhập mã: ");
        this.id = sc.nextLine();
        System.out.println("Nhập tên: ");
        this.name = sc.nextLine();
        System.out.println("Nhập tên NXB: ");
        this.publisher = sc.nextLine();
        System.out.println("Nhập ngày xuất bản: ");
        this.publishDay = sc.nextLine();
        System.out.println("Nhập số trang: ");
        this.pageAmount = sc.nextInt();
        sc.nextLine();
        System.out.println("Nhập lần danh mục xuất bản: ");
        this.publishCat = sc.nextLine();

    }

    @Override
    public void xuatInfo(){
        System.out.println("Tạp chí");
        System.out.println("Mã: "+this.id);
        System.out.println("Tên: "+this.name);
        System.out.println("NXB: "+this.publisher);
        System.out.println("Ngày xuất bản: "+this.publishDay);
        System.out.println("Số trang: "+this.pageAmount);
        System.out.println("Danh mục xuất bản: "+this.publishCat);
        System.out.println("Năm xuất bản: "+this.publishYear);
    }
}
