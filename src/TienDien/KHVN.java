package TienDien;

import java.util.Scanner;

public class KHVN extends KH {

    private Scanner sc = new Scanner(System.in);
    protected String object;
    protected int threshold;
    public KHVN(String ID, String name, String dateout, int amount, int price, String object, int threshold){
        super(ID, name, dateout, amount, price);
        this.object = object;
        this.threshold = threshold;
    }

    public KHVN(){
        super();
        this.object = "";
        this.threshold = 0;
    }

    @Override
    public void Nhap()
    {
        super.Nhap();
        System.out.println("Nhập đối tượng khách hàng (sinh hoạt, kinh doanh, sản xuất): ");
        this.object = sc.nextLine();
        System.out.println("Nhập định mức giới hạn: ");
        this.threshold =sc.nextInt();
        sc.nextLine();
    }

    @Override
    public double getPriceAll()
    {
        if (this.amount < this.threshold)
            return this.amount*this.price;
        return this.amount*this.price*this.threshold + (this.amount-this.threshold)*this.price*2.5;
    }
    @Override
    public String toString() {
        return "[Khách hàng Việt Nam: " + super.toString() + ", Đối tượng khách hàng: " + this.object + ", Định mức: " + this.threshold + ",Thành Tiền: "+ getPriceAll() + "]";
    }


}
