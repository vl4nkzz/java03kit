package TienDien;

import java.util.Scanner;

public class Main {
    public static void Menu()
    {
        System.out.println("1. Thêm khách hàng Việt Nam");
        System.out.println("2. Thêm khách hàng nước ngoài");
        System.out.println("3. Tổng số lượng từng loại khách hàng");
        System.out.println("4. Trung bình thành tiền của khách hàng người nước ngoài");
        System.out.println("5. Xuất danh sách");
        System.out.println("6. Xuất ra các hoá đơn trong tháng 09 năm 2019");
        System.out.println("7. Thoát");
        System.out.println("Mời chọn: ");
    }
    public static void main(String[] args) {
        Manager M = new Manager();
        Scanner sc = new Scanner(System.in);
        int chon=0;
        while (chon != 7) {
            Menu();
            chon = sc.nextInt();
            switch (chon) {
                case 1:
                    M.NhapKH(1);
                    break;
                case 2:
                    M.NhapKH(2);
                    break;
                case 3:
                    M.printAmount();
                    break;
                case 4:
                    M.averageOfNNPriceAll();
                    break;
                case 5:
                    M.printList();
                    break;
                case 6:
                    M.printListInMonth();
                    break;
            }
        }
    }
}
