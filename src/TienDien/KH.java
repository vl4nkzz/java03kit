package TienDien;

import java.util.Scanner;

public class KH {
    private Scanner sc = new Scanner(System.in);
    protected String ID;
    protected String name;
    protected String dateout;
    protected int amount;
    protected int price;

    public KH(String ID, String name, String dateout, int amount, int price) {
        this.ID = ID;
        this.name = name;
        this.dateout = dateout;
        this.amount = amount;
        this.price = price;
    }

    public KH() {
        this.ID = "";
        this.name = "";
        this.dateout = "";
        this.amount = 0;
        this.price = 0;
    }

    public String getDateout() {
        return dateout;
    }

    public void Nhap()
    {
        System.out.println("Nhập mã khách hàng: ");
        this.ID = sc.nextLine();
        System.out.println("Nhập Họ tên: ");
        this.name = sc.nextLine();
        System.out.println("Nhập ngày tháng (dd/mm/yyyy): ");
        this.dateout = sc.nextLine();
        System.out.println("Nhập Số KW tiêu thụ: ");
        this.amount = sc.nextInt();
        System.out.println("Nhập đơn giá: ");
        this.price =sc.nextInt();
        sc.nextLine();
    }

    @Override
    public String toString() {
        return "Mã khách hàng: " + this.ID + ", Họ tên: " + this.name + ", Ngày xuất hóa đơn: " + this.dateout + ", Số KW tiêu thụ: " + this.amount
                + ", Đơn giá: " + this.price ;
    }

    public double getPriceAll() {
        return 0;
    }
}
