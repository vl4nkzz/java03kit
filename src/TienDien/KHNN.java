package TienDien;

import java.util.Scanner;

public class KHNN extends KH {

    private Scanner sc = new Scanner(System.in);
    private String nationality;
    public KHNN(String ID, String name, String dateout, int amout, int price, String nationality) {
        super(ID,name,dateout,amout,price);
        this.nationality = nationality;
    }

    public KHNN() {
        super();
        this.nationality = "";
    }

    @Override
    public void Nhap()
    {
        super.Nhap();
        System.out.println("Nhập quốc tịch: ");
        this.nationality = sc.nextLine();
    }

    @Override
    public double getPriceAll()
    {
        return this.amount*this.price;
    }

    @Override
    public String toString() {
        return "[Khách hàng nước ngoài: " + super.toString() + ", Quốc tịch: " + this.nationality + ", Thành tiền: " + getPriceAll() + "]";
    }
}
