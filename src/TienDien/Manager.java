package TienDien;

import java.util.Scanner;

public class Manager {
    KH[] list = new KH[100];
    Scanner sc = new Scanner(System.in);
    int amountVN=0,amountNN=0,d=0;
    double priceAllNN=0;
    

    public void NhapKH(int c)
    {
        if(c==1)
        {
            list[d] = new KHVN();
            list[d].Nhap();
            amountVN++;
        }
        else
        {
            list[d] = new KHNN();
            list[d].Nhap();
            priceAllNN += list[d].getPriceAll();
            amountNN++;
        }
        d++;
    }
    public void printList()
    {
        for(int i=0; i<d; i++)
        {
            System.out.println("\n----So TT: " + (i+1));
            System.out.println(list[i].toString());
        }
    }
    public void printAmount()
    {
        System.out.println("Số khách hàng Việt Nam: " + amountVN);
        System.out.println("Số khách hàng Nước ngoài: " + amountNN);
    }
    public void averageOfNNPriceAll()
    {
        System.out.println("Trung bình thành tiền của khách hàng người nước ngoài: " + priceAllNN/amountNN );
    }
    public void printListInMonth()
    {
        for(int i=0; i<d; i++) {
            if (list[i].getDateout().indexOf("9/2019") > 0) {
                System.out.println("\n----So TT: " + (i + 1));
                System.out.println(list[i].toString());

            }
        }
    }
}
