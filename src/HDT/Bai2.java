package HDT;

import java.util.Scanner;

public class Bai2 {
    public static void menu(){
        System.out.println("1.Kiểm tra tuổi");
        System.out.println("2.Kiểm tra giới tính");
        System.out.println("3.Kiểm tra mã khoa");
        System.out.println("4.Thoát");
        System.out.println("Lựa chọn: ");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        SinhVien sv = new SinhVien();
        sv.nhapInfo();
        sv.xuatInfo();
        int rep=0;
        while (rep!=4){
            menu();
            rep = sc.nextInt();
            switch (rep){
                case 1 :
                    sv.checkAge();
                    break;
                case 2 :
                    sv.checkSex();
                    break;
                case 3 :
                    sv.checkID();
                    break;
            }
        }
    }
}
