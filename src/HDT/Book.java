package HDT;

import java.util.Scanner;

public class Book {
    private Scanner sc = new Scanner(System.in);
    private String name;
    private String publisher;
    private int pageamout;
    private int price;

    public void nhapInfo(){
        sc.nextLine();
        System.out.println("Nhập tên sách: ");
        this.name = sc.nextLine();
        System.out.println("Nhập tên nhà xuất bản: ");
        this.publisher = sc.nextLine();
        System.out.println("Nhập số trang: ");
        this.pageamout = sc.nextInt();
        System.out.println("Nhập giá: ");
        this.price = sc.nextInt();

    }

    public void xuatInfo(){
        System.out.println("Tên sách: "+this.name);
        System.out.println("Nhà xuất bản: "+this.publisher);
        System.out.println("Số trang: "+this.pageamout);
        System.out.println("Giá bán: "+this.price);
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getPublisher() {
        return publisher;
    }


}
