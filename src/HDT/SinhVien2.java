package HDT;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class SinhVien2 {
    private Scanner sc = new Scanner(System.in);
    private String name;
    private String hometown;
    private String dob;
    public void nhapInfo(){
        System.out.println("Nhâp tên: ");
        this.name = sc.nextLine();
        System.out.println("Nhập quê quán: ");
        this.hometown = sc.nextLine();
        System.out.println("Nhập năm sinh: ");
        this.dob = sc.nextLine();
    }
    public void xuatInfo(){
        System.out.println("Tên: "+this.name);
        System.out.println("Quê quán: "+this.hometown);
        System.out.println("Ngày tháng năm sinh: "+this.dob);
    }
    public void getUpcaseName(){
        System.out.println("Tên: "+this.name.toUpperCase());
        System.out.println("Quê quán"+this.hometown.toUpperCase());
    }
    public void changeInfo(){
        int rep=0;
        while (rep!=3){
            System.out.println("1.Sửa quê quán");
            System.out.println("2.Sửa ngày tháng năm sinh");
            System.out.println("3.Thoát");
            System.out.println("Lựa chọn: ");
            rep=sc.nextInt();
            switch (rep){
                case 1 :
                    System.out.println("Nhập quê quán mới: ");
                    this.hometown = sc.nextLine();
                    break;
                case 2 :
                    System.out.println("Nhập ngày tháng năm sinh mới: ");
                    this.dob = sc.nextLine();
                    break;
            }
        }
    }


    public void checkLifeTime(){
        Calendar cal = Calendar.getInstance();
        Date currentdate =cal.getTime();
        SimpleDateFormat newformat = new SimpleDateFormat("dd/MM/yyyy");
        newformat.format(currentdate);
        try {
            Date start = newformat.parse(this.dob);
            long getDiff = currentdate.getTime() - start.getTime();
            long getDaysDiff = getDiff/(24*60*60*1000);
            System.out.println("Đã sống: "+getDaysDiff+" ngày");

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
