package HDT;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class SinhVien {
    private Scanner sc = new Scanner(System.in);
    private String name;
    private String hometown;
    private int bornyear;
    private String id;
    public void nhapInfo(){
        System.out.println("Nhâp tên: ");
        this.name = sc.nextLine();
        System.out.println("Nhập quê quán: ");
        this.hometown = sc.nextLine();
        System.out.println("Nhập năm sinh: ");
        this.bornyear = sc.nextInt();
        System.out.println("Nhập mã khoa: ");
        this.id = sc.nextLine();
    }
    public void xuatInfo(){
        System.out.println("Tên: "+this.name);
        System.out.println("Quê quán: "+this.hometown);
        System.out.println("Năm sinh: "+this.bornyear);
        System.out.println("Mã khoa: "+this.id);
    }
    public void checkAge(){
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        if ((Integer.parseInt(year.format(date))-this.bornyear)>19) System.out.println("Đủ 20 tuổi");
            else System.out.println("Không đủ 20 tuổi");
    }
    public void checkSex(){
        if (this.name.contains("Thị")) System.out.println("Là nữ");
            else System.out.println("Là nam");
    }
    public void checkID(){
        switch (this.id){
            case "AT" :
                System.out.println("Khoa An toàn thông tin");
                break;
            case "CT" :
                System.out.println("Khoa Công nghệ thông tin");
                break;
            case "ĐT" :
                System.out.println("Khoa Điện tử viễn thông");
                break;
        }
    }
}
