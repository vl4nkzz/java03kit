package HDT;

import java.util.Scanner;

public class Bai4 {
    public static Scanner sc = new Scanner(System.in);
    public static void menu(){
        System.out.println("1.In thông tin");
        System.out.println("2.In tên và quê quán in hoa");
        System.out.println("3.Sửa quê/ngày tháng năm sinh");
        System.out.println("4.Số ngày sống đến hiện tại");
        System.out.println("5.Thoát");
        System.out.println("Lựa chọn: ");
    }
    public static void main(String[] args) {
        SinhVien2 sv = new SinhVien2();
        sv.nhapInfo();
        int rep=0;
        while (rep!=5){
            menu();
            rep = sc.nextInt();
            switch (rep){
                case 1 :
                    sv.xuatInfo();
                    break;
                case 2 :
                    sv.getUpcaseName();
                    break;
                case 3 :
                    sv.changeInfo();
                    break;
                case 4 :
                    sv.checkLifeTime();
                    break;
            }
        }
    }
}
