package HDT;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class HangHoa {
    private Scanner sc = new Scanner(System.in);

    private String name;
    private int amount;
    private int pricein;
    private int priceout;
    private String importdate;
    private String limitdate;
    public void nhapInfo(){
        System.out.println("Nhập tên hàng: ");
        this.name = sc.nextLine();
        System.out.println("Nhập số lượng: ");
        this.amount = sc.nextInt();
        System.out.println("Nhập giá nhập: ");
        this.pricein = sc.nextInt();
        System.out.println("Nhập giá ra: ");
        this.priceout = sc.nextInt();
        sc.nextLine();
        System.out.println("Nhập ngày nhập hàng: ");
        this.importdate = sc.nextLine();
        System.out.println("Nhập hạn sử dụng: ");
        this.limitdate = sc.nextLine();
    }
    public void xuatInfo(){
        System.out.println("Tên: "+this.name);
        System.out.println("Số lượng: "+this.amount);
        System.out.println("Giá nhập: "+this.pricein);
        System.out.println("Giá ra: "+this.priceout);
        System.out.println("Ngày nhập hàng: "+this.importdate);
        System.out.println("Hạn sử dụng: "+this.limitdate);
    }
    public void xuatTong(){
        System.out.println("Số tiền hàng nhập vào: "+this.pricein*this.amount);
        System.out.println("Số tiền hàng bán ra: "+this.priceout*this.amount);
    }
    public void checkLimitTime(){
        Calendar cal = Calendar.getInstance();
        Date currentdate =cal.getTime();
        SimpleDateFormat newformat = new SimpleDateFormat("dd/MM/yyyy");
        newformat.format(currentdate);
        try {
            Date end = newformat.parse(this.limitdate);
            long getDiff = end.getTime() - currentdate.getTime();
            long getDaysDiff = getDiff/(24*60*60*1000)+1;
            System.out.println("Hàng còn sử dụng được: "+getDaysDiff+" ngày");

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
