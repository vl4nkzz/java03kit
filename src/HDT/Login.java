package HDT;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.regex.Pattern;


public class Login {
    private Scanner sc = new Scanner(System.in);
    private String username;
    private String passwword;

    public void nhapInfo(){
        System.out.println("Nhập email: ");
        this.username = sc.nextLine();
        System.out.println("Nhập pass: ");
        this.passwword = sc.nextLine();

    }
    public void checkEmail(){
        String regex = "^[a-zA-Z][\\\\w-]+@([\\\\w]+\\\\.[\\\\w]+|[\\\\w]+\\\\.[\\\\w]{2,}\\\\.[\\\\w]{2,})$";
        if (Pattern.matches(regex, this.username)) System.out.println("Đúng");
            else System.out.println("Sai");
    }
    public static String convertByteToHex(byte[] data) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            sb.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
    private String getSHAHash(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] messageDigest = md.digest(input.getBytes());
            return convertByteToHex(messageDigest);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    public void getHash(){
        System.out.println("Email: "+getSHAHash(this.username));
        System.out.println("Password: "+getSHAHash(this.passwword));
    }
}
