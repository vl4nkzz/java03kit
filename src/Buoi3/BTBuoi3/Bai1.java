package Buoi3.BTBuoi3;

import java.util.Arrays;
import java.util.Scanner;

public class Bai1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        int a[] = new int[100];
        int i;
        System.out.println("Nhap n:");
        n= sc.nextInt();
        for (i=0; i<n ; i++){
            System.out.print("Nhap so thu "+i+" :");
            a[i]= sc.nextInt();
        }
        System.out.println("Chon cach sap xep: ");
        System.out.println("1.Tang dan");
        System.out.println("2.Giam dan");
        System.out.print("Lua chon: ");
        int k= sc.nextInt();
        switch (k){
            case 1:
                System.out.println("Ban chon sap xep tang dan.");
                Arrays.sort(a,0,n);
                System.out.print("Day sau khi sap xep");
                for(i=0; i<n ; i++){
                    System.out.print(a[i]+"   ");
                }
                break;
            case 2:
                System.out.println("Ban chon sap xep giam dan");
                Arrays.sort(a,0,n);
                System.out.print("Day sau khi sap xep: ");
                for(i=n-1; i>=0 ; i--){
                    System.out.print(a[i]+"   ");
                }
                break;
        }
    }

}
