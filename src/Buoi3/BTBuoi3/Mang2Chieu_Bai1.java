package com.company;

import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class Mang2Chieu_Bai1 {
    public static int ktnt(double a) {
        int i;
        if (a<2) return 0;
        else for (i=2;i<sqrt(a);i++)
            if (a%i==0) return 0;
        return 1;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double[][] a = new double[3][4];
        int i,j;
        for (i=0; i<3; i++){
            for (j=0; j<4; j++){
                System.out.print("Nhap a["+i+","+j+"]= ");
                a[i][j] = sc.nextDouble();
            }
        }
        System.out.println("Cac so nguyen to la: ");
        for (i=0; i<3; i++){
            for (j=0; j<4; j++){
                if (ktnt(a[i][j])==1) System.out.print(a[i][j]+"  ");
            }
        }
    }
}
