package Buoi4;

import java.util.Scanner;

public class VD3 {
    public static void ktChuoiCon(String s){
        Scanner sc = new Scanner(System.in);
        String s2;
        System.out.println("Nhập chuỗi cần ktra: ");
        s2= sc.nextLine();
        int kq= s.indexOf(s2);
        if (kq==-1) System.out.println("Không có");
            else System.out.println("Có");

    }
    public static void ktKyTu(String s){
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập kí tự cần kta: ");
        String c= sc.nextLine();
        int dem=0;
        for (int i=0; i<s.length();i++){
            if (s.charAt(i)==c.charAt(0)) dem++;
        }
        if (dem==0) System.out.println("Không có kí tự cần ktra");
        else System.out.println("Có "+dem+" kí tự trong chuỗi cần ktra");
    }
    public static void thayKiTu(String s){
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập kí tự cần kta: ");
        String c= sc.nextLine();
        System.out.println("Nhập kí tự thay đổi kí tự cũ: ");
        String x = sc.nextLine();
        String sa=s.replace(c,x);
        System.out.println("Kết quả: "+sa);

    }
    public static void menu(){
        System.out.println("1.Kiểm ra chuỗi có thuộc chuỗi ban đầu");
        System.out.println("2.Đếm kí tự");
        System.out.println("3.Thay thế kí tự");
        System.out.println("4.Thoát");
        System.out.println("Lựa chọn: ");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int c=0;
        System.out.println("Nhập chuỗi: ");
        String s = sc.nextLine();
        while (c!=5){
            menu();
            c= sc.nextInt();
            switch (c){
                case 1:
                    ktChuoiCon(s);
                    break;
                case 2:
                    ktKyTu(s);
                    break;
                case 3:
                    thayKiTu(s);
                    break;
            }
        }
    }
}
