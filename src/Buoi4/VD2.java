package Buoi4;

import java.util.Scanner;

import static java.lang.Math.sqrt;

public class VD2 {
    static int[] a = new int[100];

    public static int ktnt(float a){
        int i;
        if (a<2) return 0;
        for (i=2; i<=sqrt(a); i++){
            if (a%i==0) return 0;
        }
        return 1;
    }
    public static int nhapMang(){
        int n,i;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập số phần tử của mảng: ");
        n = sc.nextInt();
        for (i=0; i<n; i++){
            System.out.println("Nhập phần tử thứ "+i+" :");
            a[i] = sc.nextInt();
        }
        return n;
    }
    public static void xuatMang(int n){
        int i;
        System.out.print("Mảng: ");
        for (i=0; i<n; i++){
            System.out.println(a[i]+"    ");

        }
    }
    public static void ktSo(int n){
        Scanner sc = new Scanner(System.in);
        int i,x,kt=0;
        System.out.print("Nhap so can ktra: ");
        x = sc.nextInt();
        for (i=0;i<n;i++){
            if (a[i]==x) kt=1;
        }
        if (kt==1) System.out.println("Có số "+x+" trong mảng");
        else System.out.println("Không có số "+x+" trong mảng");
    }
    public static void xapxepMang(int n){
        int t;
        for (int i=0;i<n;i++){
            for (int j=0; j<n;j++){
                if(a[i]<a[j]) {
                    t=a[i];
                    a[i]=a[j];
                    a[j]=t;
                }
            }
        }
    }
    public static void inNT(int n){
        int i,kt=0;
        System.out.print("Các số nguyên tố có trong mảng: ");
        for (i=0; i<n ;i++){
            if (ktnt(a[i])==1) {
                kt=1;
                System.out.print(a[i] + "   ");
            }
        }
        System.out.println();
        if (kt==0) System.out.println("Không có số nguyên tố nào");
    }
    public static void menu(){

        System.out.println("1.Sắp xếp và hiển thị dãy số tăng dần");
        System.out.println("2.KT 1 số có trong mảng hay không");
        System.out.println("3.Hiển thị các số nguyên tố");
        System.out.println("4.Thoát");
        System.out.println("Lựa chọn: ");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int chon=0,n=nhapMang();
        while (chon!=4){
            menu();
            chon = sc.nextInt();
            switch (chon) {
                case 1:
                    xapxepMang(n);
                    xuatMang(n);
                    break;
                case 2:
                    ktSo(n);
                    break;
                case 3:
                    inNT(n);
                    break;

            }

        }

    }
}
