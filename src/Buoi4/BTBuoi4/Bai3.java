package Buoi4.BTBuoi4;

import java.util.Scanner;

import static java.lang.Character.toUpperCase;

public class Bai3 {
    public static Scanner sc = new Scanner(System.in);
    public static void menu(){
        System.out.println("1.Viết hoa toàn bộ chuỗi");
        System.out.println("2.Hex 1 kí tự");
        System.out.println("3.Viết hoa các chữ cái đầu từ");
        System.out.println("4.Thoát");
        System.out.println("Lựa chọn: ");
    }
    public static void toUpperCaseWord(String s){
        int i;
        s=" "+s;
        char t[] = s.toCharArray();
        for (i=0; i<s.length(); i++){
            if (t[i]==' ') {
                t[i+1]=toUpperCase(t[i+1]);
            }
        }
        System.out.println("Kết quả: ");
        for (i=1; i<s.length(); i++){
            System.out.print(t[i]);
            }
        System.out.println();

    }

    public static void main(String[] args) {
        int rep=0;
        System.out.println("Nhập chuỗi: ");
        String s = sc.nextLine();
        while (rep!=4){
            menu();
            rep = sc.nextInt();
            switch (rep){
                case 1 :
                    System.out.println("Chuỗi sau khi viết hoa toàn bộ: ");
                    System.out.println(s.toUpperCase());
                    break;
                case 2 :

                case 3 :
                    toUpperCaseWord(s);

            }
        }
    }
}
