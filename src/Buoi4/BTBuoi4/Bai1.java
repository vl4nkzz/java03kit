package Buoi4.BTBuoi4;

import java.util.Scanner;

public class Bai1 {
    public static int[] a = new int[100];
    public static Scanner sc = new Scanner(System.in);
    public static int nhapMang(int[] a){
        int n,i;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập số phần tử của mảng: ");
        n = sc.nextInt();
        for (i=0; i<n; i++){
            System.out.println("Nhập phần tử thứ "+i+" :");
            a[i] = sc.nextInt();
        }
        return n;
    }
    public static void xuatMang(int[] a, int n){
        int i;
        System.out.print("Mảng: ");
        for (i=0; i<n; i++){
            System.out.println(a[i]+"    ");

        }
    }
    public static int max(int[] a,int n){
        int i,max=a[0];
        for (i=0; i<n ; i++){
            if (a[i]>max) max=a[i];
        }
        return max;
    }
    public static int min(int[] a,int n){
        int i,min=a[0];
        for (i=0; i<n ; i++){
            if (a[i]<min) min=a[i];
        }
        return min;
    }
    public static void xuatSoChan(int[] a, int n){
        int i,kt=0;
        System.out.println("Các số chẵn là: ");
        for (i=0; i<n; i++){
            if (a[i]%2==0) {
                System.out.print(a[i]+"  ");
                kt=1;
            }
        }
        System.out.println();
        if (kt==0) System.out.println("Không có");
    }
    public static void xuatChiaHet(int[] a, int n){
        int i,kt=0;
        System.out.println("Các số chia hết cho 2 và 3 là: ");
        for (i=0; i<n; i++){
            if ((a[i]%2==0) && (a[i]%3==0)) {
                System.out.print(a[i]+"  ");
                kt=1;
            }
        }
        System.out.println();
        if (kt==0) System.out.println("Không có");
    }
    public static void timUocMax(int[] a, int n){
        int max=max(a,n),i,kt=0;
        System.out.println("Các số là ước của max là: ");
        for (i=0; i<n; i++){
            if (max%a[i]==0) {
                System.out.print(a[i]+"  ");
                kt=1;
            }
        }
        System.out.println();
        if (kt==0) System.out.println("Không có");
    }
    public static void timBoiMin(int[] a, int n){
        int min=min(a,n),i,kt=0;
        System.out.println("Các số là bội của min là: ");
        for (i=0; i<n; i++){
            if (a[i]%min==0) {
                System.out.print(a[i]+"  ");
                kt=1;
            }
        }
        System.out.println();
        if (kt==0) System.out.println("Không có");
    }
    public static void menu(){
        System.out.println("1.Tìm Max, Min");
        System.out.println("2.In các số chẵn");
        System.out.println("3.In các số chia hết cho 2 và 3");
        System.out.println("4.In các số là ước của Max");
        System.out.println("5.In các số là bội của Min");
        System.out.println("6.Thoát");
        System.out.println("Lựa chọn: ");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int rep=0;
        int n;
        n=nhapMang(a);
        while (rep!=6){
            menu();
            rep= sc.nextInt();
           switch (rep){
               case 1 :
                   System.out.println("Max: "+max(a, n));
                   System.out.println("Min: "+min(a, n));
                   break;
               case 2 :
                   xuatSoChan(a, n);
                   break;
               case 3 :
                   xuatChiaHet(a,n);
                   break;
               case 4 :
                   timUocMax(a, n);
                   break;
               case 5 :
                   timBoiMin(a, n);
                   break;
           }
        }


    }
}
