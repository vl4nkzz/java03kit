package Buoi4.BTBuoi4;

import java.util.Scanner;

import static java.lang.StrictMath.sqrt;

public class Bai2 {
    public static int[][] a = new int[100][100];
    public static int n;
    public static int m;
    public static Scanner sc = new Scanner(System.in);

    public static int checkPrime(int a){
        int i;
        if (a<2) return 0;
        if (a==2) return 1;
        for (i=2; i<=sqrt(a); i++){
            if (a%i==0) return 0;
        }
        return 1;
    }
    public static int checkSquareRoot(int a){
        if ((sqrt(a)*sqrt(a))==a) return 1;
        return 0;
    }
    public static void printMainLine(int[][] a, int m, int n){
        int i,j;
        System.out.println(m+","+n);
        System.out.println("Các phần tử thuộc đường chéo chính: ");
        for (i=0; i<m; i++){
            for (j=0; j<n ; j++){
                if (i==j) System.out.print(a[i][j]+"  ");
            }
        }
        System.out.println();
    }
    public static void printPrime(int[][] a, int m, int n){
        int i,kt=0;
        System.out.println("Các số nguyên tố thuộc hàng "+m+" :");
        for (i=0; i<n; i++){
            if (checkPrime(a[m][i])==1){
                System.out.print(a[m][i]+"  ");
                kt=1;
            }
        }
        System.out.println();
        if (kt==0) System.out.println("Không có");
    }
    public static void printSquareRoot(int[][] a, int m, int n) {
        int j, kt = 0;
        System.out.println("Các số chính phương thuộc cột " + n + " :");
        for (j = 0; j < n; j++) {
            if (checkSquareRoot(a[j][n]) == 1) {
                System.out.print(a[j][n] + "  ");
                kt = 1;
            }
        }
        System.out.println();
        if (kt == 0) System.out.println("Không có");
    }

    public static int calcSumRow(int[][] a, int m, int n){
        int i,s=0;
        for(i=0;i<n;i++){
            s+=a[m][i];
        }
        return s;
    }
    public static void menu(){
        System.out.println("1.In các phần tử thuộc đường chéo chính");
        System.out.println("2.In danh sách các số nguyên tố thuộc 1 hàng");
        System.out.println("3.In danh sách các số chính phương thuộc 1 cột");
        System.out.println("4.Tính tổng các phần tử thuộc 1 hàng");
        System.out.println("5.Thoát");
        System.out.println("Lựa chọn: ");
    }
    public static void main(String[] args) {
        int rep=0,t;
        int i,j;
        System.out.println("Nhập số hàng: ");
        n= sc.nextInt();
        System.out.println("Nhập số cột: ");
        m= sc.nextInt();
        for (i=0; i<m; i++){
            for (j=0; j<n; j++){
                System.out.println("Nhập phần tử ("+i+","+j+") :");
                a[i][j]= sc.nextInt();
            }
        }
        while (rep!=5){
            menu();
            rep = sc.nextInt();
            switch (rep){
                case 1 :
                    printMainLine(a,m,n);
                    break;
                case 2 :
                    System.out.print("Nhập hàng: ");
                    t = sc.nextInt();
                    printPrime(a,t,n);
                    break;
                case 3 :
                    System.out.println("Nhập cột: ");
                    t = sc.nextInt();
                    printSquareRoot(a,m,t);
                    break;
                case 4 :
                    System.out.println("Nhập hàng: ");
                    t = sc.nextInt();
                    System.out.println("Tông: "+calcSumRow(a,t,n));
                    break;
            }
        }
    }
}
